package main

import "github.com/graphql-go/graphql"

func NewSchema(rootQuery *graphql.Object, rootMutation *graphql.Object) graphql.Schema {
	schema, _ := graphql.NewSchema(graphql.SchemaConfig{
		Query:    rootQuery,
		Mutation: rootMutation,
	})
	return schema
}